import { ThemeProvider } from 'styled-components';
import theme from '../src/theme';
import Head from 'next/head'
import { GlobalStyle } from '../src/theme/GlobalStyle';


export default function App({ Component, pageProps }) {
  return (
    <>
      <Head>
        <title>Webjump</title>
        <link rel="preconnect" href="https://fonts.googleapis.com"/>
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin/>
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;700;800&display=swap" rel="stylesheet"/>
      </Head>
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <Component {...pageProps} />
      </ThemeProvider>
    </>
  )
}
