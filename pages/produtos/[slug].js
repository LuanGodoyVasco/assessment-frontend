import React from 'react';
import Head from 'next/head';
import { api } from '../../services/api';

import Text from '../../src/components/foundation/Text';
import AsideFilterProduct from '../../src/components/commons/AsideFilterProduct';
import { Grid } from '../../src/components/foundation/layout/Grid';
import CardProduto from '../../src/components/commons/CardProduto';
import Layout from '../../src/components/foundation/layout';

export default function Produto({ produtos }) {
  return (
    <Layout>
      <Head>
        <title>{produtos.title} |  Webjump </title>
      </Head>
      <Grid.Col
        value={{ xs: 12, md: 4, lg: 3 }}
      >
        <AsideFilterProduct produtos={produtos}/>
      </Grid.Col>  
      <Grid.Col
        value={{ xs: 12, md: 8, lg: 9 }}
      >
        <main>
          <Text
            tag="h1"
            variant="title"
            color="secondary.main"
            textAlign={{
              xs: 'center',
              sm: 'left',
            }}
          >
            {produtos.title}
          </Text>
          <Grid.Row>
            {produtos.items.map((item) => {
              return (
                <Grid.Col
                  value={{ xs: 6, md: 4, lg: 3 }}
                  key={item.id}
                >
                  <article>
                    <CardProduto item={item}/>
                  </article>
                </Grid.Col>
              )
            })}
            </Grid.Row>
        </main>
      </Grid.Col>
    </Layout>
  )
}

export async function getStaticPaths() {
  const { data } = await api.get(`produtos`)
  
  const paths = data.map(produto => {
    return {
      params: {
        slug: produto.name
      }
    }
  })
  return {
    paths,
    fallback: 'blocking'
  }
}
export async function getStaticProps(ctx) {
  const { slug } = ctx.params;
  const { data } = await api.get(`produtos/?name=${slug}`)
  
  const produtos = data.map(produto => {
    return {
      title: produto.title,
      name: produto.name,
      filters: produto.filters,
      items: produto.items,
    };
  })

  return {
    props: {
      produtos: produtos[0],
    },
  }
}