import React from 'react';
import Head from 'next/head';
import Text from '../src/components/foundation/Text';
import AsideNav from '../src/components/commons/AsideNav';
import Banner from '../src/components/commons/Banner';
import { Grid } from '../src/components/foundation/layout/Grid';
import Layout from '../src/components/foundation/layout';

export default function Home() {
  return (
    <Layout>
      <Head>
        <title>Home | Webjump </title>
      </Head>
      <Grid.Col
        value={{ xs: 12, md: 4, lg: 3 }}
      >
        <AsideNav 
          display={{
            xs: 'none',
            sm: 'block',
          }}
        />
      </Grid.Col>  
      <Grid.Col
        value={{ xs: 12, md: 8, lg: 9 }}
      >
        <main>
          <Banner />
          <Text
            tag="h1"
            variant="title"
            color="secondary.main"
            textAlign={{
              xs: 'center',
              sm: 'left',
            }}
          >
            Seja bem-vindo!
          </Text>
          <Text 
            tag="p"
            variant="paragraph1"
            color="secondary.light"
            textAlign={{
              xs: 'center',
              sm: 'left',
            }}
          >
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam minima quos doloribus molestiae porro qui eum aspernatur ratione numquam tenetur, corporis similique perferendis esse aut eius dolore odio eos temporibus.
            Sapiente error debitis iusto fuga et dignissimos, sunt tenetur voluptate quisquam minima veritatis illum nesciunt iure ducimus labore numquam amet nostrum quasi a quo laboriosam odit aspernatur? Pariatur, cumque earum!
            Quia, consequatur corporis a quam ipsum quo maiores commodi et, sunt placeat itaque culpa voluptas non totam molestias vel libero corrupti, ullam perferendis aliquid. Repudiandae facere libero veniam laboriosam vel.
            Accusantium modi harum iusto reprehenderit impedit atque voluptatum laudantium totam facere blanditiis amet fuga labore, ea nobis architecto eos, aliquid tempore exercitationem velit obcaecati mollitia placeat similique dicta quisquam! Voluptatibus?
            Inventore repellat ex quidem facilis praesentium nobis fuga, quae in maxime dolore placeat voluptate doloremque quisquam consequuntur vel corporis quasi odio sint molestias voluptatibus autem ullam voluptates? Ratione, dolore voluptates?
          </Text>
        </main>
      </Grid.Col>
    </Layout>
  );
}
