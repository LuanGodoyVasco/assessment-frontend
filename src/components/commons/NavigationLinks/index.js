import React, { useState, useEffect } from 'react';
import Link from 'next/link';

import Text from '../../foundation/Text';
import { api } from '../../../../services/api';

export default function NavigationLinks({ variant, color, marginRight, marginBottom, justCategories, ...props}) {
  const [categories, setCategories] = useState([]);
  const fetchCategories = async () => {
    try {
      const { data } = await api.get(`/list`);     
      data.map(item => {
        let path = item.path
        item.path = item.id ?  `/produtos/${path}` : ''
      })
      if(justCategories) {
        setCategories(data);
      } else {
        data.unshift({ name: 'Página inicial', path: '/' })
        data.push({ name: 'Contato', path: '/contato' })
      }
      setCategories(data);
    } catch (err) {
        console.log(err)
    }
  };
  useEffect(() => {
    fetchCategories();
  }, []);
  return (
    <>
      {categories.map((link, index) => {
        return (
          <Text 
            tag="li"
            key={index} 
            marginRight={marginRight}
            marginBottom={marginBottom}
            color={ color }
          >
            <Link href={link.path}>
              <a>
                <Text 
                  tag="span" 
                  variant={ variant } 
                  color={ color }
                  { ...props }
                  >
                  {link.name}
                </Text>
              </a>
            </Link>
          </Text>
        )
      })}
    </>
  )
}
