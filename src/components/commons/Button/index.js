import styled, { css } from 'styled-components';
import get from 'lodash/get';
import { TextStyleVariantsMap } from '../../foundation/Text';
import { breakpointsMedia } from '../../../theme/utils/breakpointsMedia';
import { propToStyle } from '../../../theme/utils/propToStyle';

const ButtonGhost = css`
  color: ${(props) => get(props.theme, `colors.${props.variant}.color`)};
  background: transparent;
`;

const ButtonDefault = css`
  color: ${(props) => get(props.theme, `colors.${props.variant}.contrastText`)};
  background-color: ${(props) => get(props.theme, `colors.${props.variant}.color`)};
`;

export const Button = styled.button`
  width: 100%;
  height: 40px;
  max-width: 210px;
  border: none;
  cursor: pointer;
  opacity: 1;
  ${(props) => {
    if(props.ghost) {
      return ButtonGhost;
    }
    return ButtonDefault;
  }}
  transition: opacity ${({ theme }) => theme.transition};
  border-radius: ${(props) => props.variant == 'tertiary.main' ? props.theme.borderRadius : 'initial'};
  &:hover,
  &:focus {
    opacity: inherit.5s;
  }
  ${breakpointsMedia({
    xs: css`
      ${TextStyleVariantsMap.button}
    `,
    md: css`
      ${TextStyleVariantsMap.button}
    `,
  })}
  ${propToStyle('display')}
  ${propToStyle('textTransform')}
`;