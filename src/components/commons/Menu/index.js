import React from 'react';
import Link from 'next/link';
import { MenuWrapper } from './styles/MenuWrapper';
import { Logo } from '../../../theme/Logo';
import { Button } from '../../commons/Button';
import Search from '../../commons/Search';
import Text from '../../foundation/Text';
import { Grid } from '../../foundation/layout/Grid';
import NavigationLinks from '../NavigationLinks';

export default function Menu({ categories }) {
  return (
    <MenuWrapper>
      <MenuWrapper.TopSide>
        <Grid.Container>
          <Grid.Row>
            <Grid.Col 
              display='flex'
              justifyContent={{ xs: 'center', sm: 'flex-end' }}
              alignItems='center'
              value={{ xs: 12, md: 12 }}
            >
              <Text 
                tag="a" 
                variant="linkXs" 
              >
                Acesse sua Conta
              </Text>
              <Text 
                tag="span"
                variant="paragraph2"
                margin='0 0.5rem'
              >
                ou
              </Text>
              <Text
                tag="a" 
                variant="linkXs" 
              >
                Cadastre-se
              </Text>
            </Grid.Col>
          </Grid.Row> 
        </Grid.Container>
      </MenuWrapper.TopSide>
      <Grid.Container>
        <Grid.Row
          justifyContent='spaceBetween'
          alignItems='center'
          margin='2.5rem 0'
        >
          <Grid.Col
            value={{ xs: 3, md: 0 }}
            display={{ xs: 'block', sm: 'none' }}            
          >
            <MenuWrapper.LeftSide>
            <a>
              <span></span>
              <span></span>
              <span></span>
            </a>
          </MenuWrapper.LeftSide>
          </Grid.Col>
          <Grid.Col
              value={{ xs: 6, md: 4, lg: 6, xl: 8 }}
            >
            <MenuWrapper.CentralSide>
              <Logo />
            </MenuWrapper.CentralSide>
          </Grid.Col>
          <Grid.Col
            value={{ xs: 3, md: 8, lg: 6, xl: 4 }}
          >
            <MenuWrapper.RightSide>
          <Button 
            ghost
            display={{
              xs: 'block',
              sm: 'none',
            }}
          >
            <img 
              src="/icons/search.png" 
              alt="Pesquisar" 
              title="Pesquisar" 
              width="28"
              height="28"
            />
          </Button>
          <Search 
            display={{
              xs: 'none',
              sm: 'block',
            }}
          />
        </MenuWrapper.RightSide>
          </Grid.Col>
        </Grid.Row>
      </Grid.Container>
      <MenuWrapper.BottomSide
        display={{ xs: 'none', sm: 'flex' }}       
      >
        <Grid.Container>
          <Grid.Row>
            <Grid.Col
              display='flex'
              alignItems='center'
              justifyContent='spaceBetween'
              value={{ xs: 12, md: 12 }}
            >
              <NavigationLinks 
                variant="link"
                color="primary.main"
                textTransform="uppercase"
                marginRight={{ sm: '3rem', lg: '4rem' }}
              />
            </Grid.Col>
          </Grid.Row>
        </Grid.Container>
      </MenuWrapper.BottomSide>
    </MenuWrapper>
  )
}

