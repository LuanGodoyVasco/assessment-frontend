import styled from 'styled-components';
import { propToStyle } from '../../../../theme/utils/propToStyle';

export const MenuWrapper = styled.nav`
  padding: 0;
`;

MenuWrapper.TopSide = styled.div`
  color: ${(props) =>  props.theme.colors.primary.main.contrastText};
  background-color: ${(props) =>  props.theme.colors.background.tertiary.color};
  order: 1;
  height: 30px;
  width: 100%;
  display: flex;
  align-items: center;
`;

MenuWrapper.LeftSide = styled.div`
  order: 2;
  a {
    height: 20px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    span {
      width: 22px;
      height: 4px;
      background-color: ${(props) =>  props.theme.colors.background.tertiary.color};
    }
  }
`;

MenuWrapper.CentralSide = styled.div`
  padding: 0;
  margin: 0;
  order: 3;
`;

MenuWrapper.RightSide = styled.div`
  order: 4;
`;

MenuWrapper.BottomSide = styled.div`
  ${propToStyle('display')}
  
  background-color: #cc0d1f;
  color: #fff;
  width: 100%;
  order: 5;
  padding: 1rem;
  li {
    list-style: none;
    text-align: center;
  }
`;