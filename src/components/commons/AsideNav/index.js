import React from 'react';
import styled from 'styled-components';
import { propToStyle } from '../../../theme/utils/propToStyle';
import NavigationLinks from '../NavigationLinks';

export const AsideWrapper = styled.nav`
  height: 100%;
  width: 100%;
  min-height: 480px;
  min-width: 208px;
  padding-top: 0.5rem;
  color: ${({ theme }) => theme.colors.secondary.main.contrastText};
  background-color: ${({ theme }) => theme.colors.secondary.main.color};
  ${propToStyle('display')}
  li {
    margin-bottom: 1rem;
    &:last-child {
      margin-bottom: 0;
    }
  }
`;

export default function AsideNav ({ ...props }) {
  return (
    <AsideWrapper
      {...props}
    >
      <ul>
        <NavigationLinks 
          variant="paragraph"
          color="secondary.main"
        />
      </ul>
    </AsideWrapper>
  );
}