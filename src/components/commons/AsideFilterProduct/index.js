import React from 'react'
import styled from 'styled-components';
import { propToStyle } from '../../../theme/utils/propToStyle';
import Text from '../../foundation/Text';
import NavigationLinks from '../NavigationLinks';
import Filters from '../Filters';

export const AsideFilterProduct = styled.nav`
  height: 100%;
  width: 100%;
  min-height: 480px;
  padding: 0 2rem;
  color: ${({ theme }) => theme.colors.secondary.main.contrastText};
  border: 1px solid ${({ theme }) => theme.colors.secondary.main.color};
  ${propToStyle('display')}
`;

export default function AsideNav ({ produtos }) {
  return (
    <AsideFilterProduct>
      <Text 
        tag="p"
        variant="subTitle"
        color="primary.red"
        textTransform="uppercase"
      >
        Filtre por
      </Text>
      <Text 
        tag="p"
        variant="subTitleXs"
        color="secondary.light"
        textTransform="uppercase"
      >
        Categorias
      </Text>
      <NavigationLinks 
        variant="paragraph"
        color="secondary.light"
        marginBottom="0.875rem"
        justCategories
      />
      <Text 
          tag="p"
          variant="subTitleXs"
          color="secondary.light"
          textTransform="uppercase"
        >
          <Filters 
            filter={produtos.filters[0]}
            produtos={produtos.items}
          />
        </Text>
    </AsideFilterProduct>
  );
}