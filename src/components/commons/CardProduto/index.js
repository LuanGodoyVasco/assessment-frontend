import React from 'react'
import styled from 'styled-components'

import Text from '../../foundation/Text';
import { Button } from '../Button';
import { api } from '../../../../services/api';
import { Grid } from '../../foundation/layout/Grid';

export const CardWrapper = styled.div`
  padding: 0;
  margin-bottom: 1.563rem;
  text-align: center;
  height: 100%;
  img {
    object-fit: contain;
    margin: 0 auto;
    border: 1px solid ${({ theme }) => theme.colors.secondary.main.color};
    width: 140px;
    height: 140px;
  }
  @media(min-width: 1024px) {
    img { 
      width: 212px; 
      height: 230px;
    }
  }
`;

export default function CardProduto({ item }) {
  return (
    <CardWrapper>
      <img 
        src={process.env.baseUrl + item.image} 
        width="212" 
        height="230"
        alt={item.name}
      />
      <div>
        <Text
          tag="h2"
          variant="paragraph"
          color="secondary.light"
          textTransform="uppercase"
          height={{xs:"50px", md:"40px"}}
          display="-webkit-box"
          webkitBoxOrient="vertical"
          webkitLineClamp="2"
          textOverflow="ellipsis"
          overflow="hidden"
          title={item.name}
          >
          {item.name}
        </Text>
        <Grid.Row
          alignItems="center"
          margin='0 0 1.125rem 0'
        >
          <Grid.Col
            display={item.specialPrice ? '' : 'none'}
            value={{ xs: '12', lg: '5' }}
          >
            <Text
              tag="span"
              variant="paragraph"
              color="quaternary.main" 
              margin="0"
            >
              <s>{item.specialPrice ? item.specialPrice.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'}) : ''}</s>
            </Text>
          </Grid.Col>
          <Grid.Col
            value={{ 
              xs: '12', 
              lg: item.specialPrice ? '7' : '12',
            }}
          >
            <Text
              tag="p"
              variant="subTitle"
              color="primary.red" 
              margin="0"             
            >
              {item.price.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'})}
            </Text>
          </Grid.Col>
        </Grid.Row>
      </div>
      <Button
        variant="tertiary.main"
        textTransform="uppercase"
      >
        Comprar
      </Button>
    </CardWrapper>
  );
}