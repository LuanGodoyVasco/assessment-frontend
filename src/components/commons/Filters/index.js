import React from 'react'
import styled from 'styled-components';
import { Grid } from '../../foundation/layout/Grid'
import Text from '../../foundation/Text'

Filters.Color = styled.label`
  cursor: pointer;
  input { display:none; } 
  input[type="radio"]:checked + span { 
    box-shadow: inset 0 0 0 2px white;
  }
  span {
    display:inline-block;
    vertical-align: middle;
    height: 30px;
    width:30px;
    margin: 10px;
    border: 1px solid #d4d4d4;
  }

`;
function onlyUnique(value, index, self) {
  return self.indexOf(value) === index;
}
export default function Filters({ filter, produtos }) {
  const haveColors = filter.color
  return (
    <div>
      {haveColors 
      ? <Colors 
          produtos={produtos}
          filter={filter}
        /> 
      : <Genders 
          produtos={produtos} 
          filter={filter}  
        />
      }
    </div>
  )
}


function Genders({filter, produtos}) {
  let genders = [];
  {produtos.map((item) => {
      item.filter.map((type) => {
        genders.push(type.gender)
      })
    })
  }
  let typeGenders = genders.filter(onlyUnique);
  return (
    <>
      <Text 
        tag="p"
        variant="subTitleXs"
        color="secondary.light"
        textTransform="uppercase"
      >
      {filter.gender}
      </Text>
      <Grid.Row>
        <Grid.Col>
          {typeGenders.map(item => {
              return(
                <Text
                  tag="li"
                  marginBottom="0.875rem"
                >
                  {item}
                </Text>
              )
            })
          }
        </Grid.Col>
      </Grid.Row>
    </>
  );
}

function Colors({filter, produtos}) {
  let colors = [];
 
  produtos.map((item) => {
    item.filter.map(item => {
      colors.push(item.color)
    })
  });
  
  let typeColors = colors.filter(onlyUnique);
  return (
    <>
      <Text 
        tag="p"
        variant="subTitleXs"
        color="secondary.light"
        textTransform="uppercase"
      >
      {filter.color}
      </Text>
      <Grid.Row >
        {typeColors.map(color => {
            return (
              <Grid.Col 
                value={{ xs: 3, md: 4, lg: 3}}
                key={color}
              >
                <Filters.Color>
                  <input type="radio" name="color" value={color.toLowerCase()}/>
                  <Text tag="span" backgroundColor={color.toLowerCase()}></Text> 
                </Filters.Color >
              </Grid.Col>
            )
          })
        }            
      </Grid.Row>
    </>
  );
}
