import React from 'react';
import styled from 'styled-components';
import { Button } from '../Button';
import { propToStyle } from '../../../theme/utils/propToStyle';

export const SearchWrapper = styled.form`
  input[type="text"] {
    height: 40px;
    width: 100%;
    box-sizing: border-box;
    border: 1px solid #959595;
    padding: 0;
  }
  @media(min-width: 768px) { 
    input[type="text"] {
      width: 304px;
    }
    button {
      max-width: 110px;
    }
  }
  ${propToStyle('display')}
`;
export default function Search({ ...props }) {
  return (
    <SearchWrapper
      {...props}
    >
        <input type="text" name="search" id="search"/>
        <Button variant="primary.main">
          Buscar
        </Button>
    </SearchWrapper>
  );
}