import React from 'react';
import styled from 'styled-components';
import { Grid } from '../../foundation/layout/Grid';

export const FooterWrapper = styled.footer`
  height: 176px;
  width: 100%;
  margin: 0 auto 30px auto;
  background-color: ${({ theme }) => theme.colors.background.main.color};
`;

export default function Footer () {
  return (
    <Grid.Container>
      <FooterWrapper/>
    </Grid.Container>
  );
}