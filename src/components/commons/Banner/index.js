import React from 'react';
import styled from 'styled-components';

export const BannerWrapper = styled.section`
  height: 325px;
  width: 100%;
  background-color: ${({ theme }) => theme.colors.background.secondary.color};
`;

export default function Banner () {
  return (
    <BannerWrapper>
    </BannerWrapper>
  );
}