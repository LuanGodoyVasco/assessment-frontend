import react from 'react';
import get from 'lodash/get';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import { propToStyle } from '../../../theme/utils/propToStyle';

export const TextStyleVariantsMap = {
  title: css`
    font-size: ${({ theme }) => theme.typographyVariants.title.fontSize};
    font-weight: ${({ theme }) => theme.typographyVariants.title.fontWeight};
    line-height: ${({ theme }) => theme.typographyVariants.title.lineHeight};
  `,
  subTitle: css`
    font-size: ${({ theme }) => theme.typographyVariants.subTitle.fontSize};
    font-weight: ${({ theme }) => theme.typographyVariants.subTitle.fontWeight};
    line-height: ${({ theme }) => theme.typographyVariants.subTitle.lineHeight};
  `,
  titleXs: css`
    font-size: ${({ theme }) => theme.typographyVariants.titleXs.fontSize};
    font-weight: ${({ theme }) => theme.typographyVariants.titleXs.fontWeight};
    line-height: ${({ theme }) => theme.typographyVariants.titleXs.lineHeight};
  `,
  subTitleXs: css`
    font-size: ${({ theme }) => theme.typographyVariants.subTitleXs.fontSize};
    font-weight: ${({ theme }) => theme.typographyVariants.subTitleXs.fontWeight};
    line-height: ${({ theme }) => theme.typographyVariants.subTitleXs.lineHeight};
  `,
  link: css`
    font-size: ${({ theme }) => theme.typographyVariants.link.fontSize};
    font-weight: ${({ theme }) => theme.typographyVariants.link.fontWeight};
    line-height: ${({ theme }) => theme.typographyVariants.link.lineHeight};
  `,
  linkXs: css`
    font-size: ${({ theme }) => theme.typographyVariants.linkXs.fontSize};
    font-weight: ${({ theme }) => theme.typographyVariants.linkXs.fontWeight};
    line-height: ${({ theme }) => theme.typographyVariants.linkXs.lineHeight};
  `,
  button: css`
    font-size: ${({ theme }) => theme.typographyVariants.button.fontSize};
    font-weight: ${({ theme }) => theme.typographyVariants.button.fontWeight};
    line-height: ${({ theme }) => theme.typographyVariants.button.lineHeight};
  `,
  paragraph: css`
    font-size: ${({ theme }) => theme.typographyVariants.paragraph.fontSize};
    font-weight: ${({ theme }) => theme.typographyVariants.paragraph.fontWeight};
    line-height: ${({ theme }) => theme.typographyVariants.paragraph.lineHeight};
  `,
  paragraph1: css`
    font-size: ${({ theme }) => theme.typographyVariants.paragraph1.fontSize};
    font-weight: ${({ theme }) => theme.typographyVariants.paragraph1.fontWeight};
    line-height: ${({ theme }) => theme.typographyVariants.paragraph1.lineHeight};
  `,
  paragraph2: css`
    font-size: ${({ theme }) => theme.typographyVariants.paragraph2.fontSize};
    font-weight: ${({ theme }) => theme.typographyVariants.paragraph2.fontWeight};
    line-height: ${({ theme }) => theme.typographyVariants.paragraph2.lineHeight};
  `,
  smallestException: css`
    font-size: ${({ theme }) => theme.typographyVariants.smallestException.fontSize};
    font-weight: ${({ theme }) => theme.typographyVariants.smallestException.fontWeight};
    line-height: ${({ theme }) => theme.typographyVariants.smallestException.lineHeight};
  `,
}

const TextBase = styled.span`
  ${(props) => TextStyleVariantsMap[props.variant]}
  background-color: ${(props) => get(props.theme, `colors.${props.backgroundColor}`)};
  ${propToStyle('textTransform')}
  ${propToStyle('height')}
  ${propToStyle('margin')}
  ${propToStyle('marginBottom')}
  ${propToStyle('marginRight')}
  ${propToStyle('display')}
  -webkit-box-orient: ${(props) => props.webkitBoxOrient };
  -webkit-line-clamp: ${(props) => props.webkitLineClamp };
  ${propToStyle('text-overflow')}
  ${propToStyle('overflow')}
  color: ${(props) => get(props.theme, `colors.${props.color}.contrastText`)};
`;

export default function Text({ tag, variant, children, ...props }) {
  return (
    <TextBase
      as={tag}
      variant={variant}
      {...props}
    >
      {children}
    </TextBase>
  );
}

Text.propTypes = {
  tag: PropTypes.string,
  variant: PropTypes.string,
  children: PropTypes.node,
}

Text.defaultProps = {
  tag: 'span',
  variant: 'paragraph1',
  children: null,
}