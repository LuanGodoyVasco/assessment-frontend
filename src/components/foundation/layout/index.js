import React from 'react'
import Head from 'next/head';
import Menu from '../../commons/Menu';
import Footer from '../../commons/Footer';
import { Box } from './Box';
import { Grid } from './Grid';

export default function Layout({ children }) { 
  return (
    <Box 
      flex='1'
      display='flex'
      flexWrap='wrap'
      flexDirection='column'
      justifyContent='space-between'
    >
      <Head>
        <title>Webjump</title>
      </Head>
      <Menu/>
      <Grid.Container flex="1">
        <Grid.Row  
          margin="3rem 0"
        >
          {children}
        </Grid.Row>
      </Grid.Container>
      <Footer />
    </Box>
  )
}
