import { typographyVariants } from './typographyVariants';

export const colors = {
  background: {
    main: {
      color: '#cc0d1f',
    },
    secondary: {
      color: '#acacac',
    },
    tertiary: {
      color: '#231f20',
    }
  },
  primary: {
    main: { //Botao principal, Menu e Titulos
      color: '#cc0d1f',
      contrastText: '#fffefd',
    },
    red: { 
      color: '#fffefd',
      contrastText: '#cc0d1f',
    },
  },
  secondary: {
    main: {  //Sidebar Home e Titulo
      color: '#e2dedb',
      contrastText: '#000000',
    },
    light: {  //Sidebar Produtos, Texto e nome produto
      color: '#ffffff',
      contrastText: '#646464',
    }
  },
  tertiary: {
    main: { //Botao Produto
      color: '#80bdb8',
      contrastText: '#f8fefa',
    },
    dark: { //Sidebar Cor Parecida com Botao de Produto
      color: '#ffffff',
      contrastText: '#62908d',
    }
  },
  quaternary: { //Desconto do produto
    main: {
      contrastText: '#808082',
    }
  },
  laranja: 'orange',
  preta: 'black',
  preto: 'black',
  rosa: 'pink',
  bege: 'beige',
  azul: 'blue',
  amarela: 'yellow',
  cinza: 'gray',
}

export default {
  colors,
  typographyVariants,
  breakpoints: {
    xs: 0,
    sm: 460,
    md: 768,
    lg: 1024,
    xl: 1440,
  },
  fontFamily: '\'Open Sans\', sans-serif',
  borderRadius: '8px',
  transition: '200ms ease-in-out',
}