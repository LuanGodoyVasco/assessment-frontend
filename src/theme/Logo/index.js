import React from 'react';

export function Logo() {
  return (
    <img 
      src="/logo_webjump.png" 
      alt="Webjump" 
      title="Webjump" 
      width="120"
      height="36"
    />
  )
}