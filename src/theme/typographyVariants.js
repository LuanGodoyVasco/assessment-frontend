export const typographyVariants = {
  // [title]
  title: {
    fontSize: '32px',
    fontWeight: '400',
    lineHeight: 1.25, // 125%
  },
  titleXs: {
    fontSize: '24px',
    fontWeight: '400',
    lineHeight: 1.25, // 125%
  },
  subTitle: {
    fontSize: '24px',
    fontWeight: '800',
    lineHeight: 1.25, // 125%
  },
  subTitleXs: {
    fontSize: '18px',
    fontWeight: '600',
    lineHeight: 1.25, // 125%
  },
  // [link]
  link: {
    fontSize: '16px',
    fontWeight: '600',
    lineHeight: 1.25,
  },
  linkXs: {
    fontSize: '14px',
    fontWeight: '600',
    lineHeight: 1,
  },
  button: {
    fontSize: '18px',
    fontWeight: '800',
    lineHeight: 1.25,
  },
  // [paragraph1]
  paragraph: {
    fontSize: '18px',
    fontWeight: '400',
    lineHeight: 1.25, // 125%
  },
  paragraph1: {
    fontSize: '16px',
    fontWeight: '400',
    lineHeight: 1.25, // 125%
  },
  // [paragraph2]
  paragraph2: {
    fontSize: '14px',
    fontWeight: '400',
    lineHeight: 1, 
  },
  // [smallestException]
  smallestException: {
    fontSize: '12px',
    fontWeight: '400',
    lineHeight: 1, // 100%
  },
}; 
